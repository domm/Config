" Adapted and sane defaults
" hmu domm@mpcdf.mpg.de

""""""""""""""""""""""""""""

" Basics {{{1 

set nocompatible

" load defaults
set hlsearch
if &term =~? 'mlterm\|xterm'
	set t_Co=256
endif

" custom colors from .Xresources
highlight LineNr term=underline ctermfg=3 guifg=Orange
highlight CursorLineNr term=underline ctermfg=13 guifg=Purple
highlight Statement term=bold ctermfg=3 gui=bold guifg=Orange
highlight Search term=reverse ctermfg=10 ctermbg=8 guifg=Black guibg=Yellow 
highlight Comment term=bold ctermfg=2 guifg=#80a0ff

" '\ + c' toogles Cursorline
highlight CursorLine term=underline 
nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>

" vim clipboard integration
set clipboard^=unnamedplus

" line numbers
set number
set relativenumber
set numberwidth=1

" ask
set confirm

" enable tab completion 
set wildmenu

set shortmess+=aAcIws   " Hide certain messages 

set expandtab           " Tab inserts Spaces
set softtabstop=2       " Amount of Spaces
set shiftwidth=2        " 2 Space indentation

" enable mouse
set mouse=a
if has('mouse_sgr')
    set ttymouse=sgr
endif

" syntax highlighting with true colors 
syntax on

" thx zama

set title
"set undofile
set encoding=utf-8
set scrolloff=5
set showmode
set ttyfast
set ignorecase
set smartcase
set incsearch
set showmatch
set wrap
set linebreak
set nolist
set formatoptions=qrn1tcj
set pastetoggle=<F2>
set laststatus=2            "airline
set splitbelow
set splitright
set foldmethod=indent
set foldopen -=block
set foldnestmax=5
set backspace=indent,eol,start
set lazyredraw
set modelines=1
set conceallevel=2
set concealcursor=vc
set updatetime=250
set noshowmode
set wildignore+=*/vendor/*,*/vagrant/*,*.so,*.swp,*.zip
 
" Keep swap and undo files in central location to avoid polluting the
" filesystem
set directory=~/.vim/swapfiles//
set undodir=~/.vim/undodir

" grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" Search with CtrlSF
nnoremap <C-F> :CtrlSF 

" Eliminate delay after pressing ESC to go to Normal mode
" https://www.johnhawthorn.com/2012/09/vi-escape-delays/
set timeoutlen=1000 ttimeoutlen=0

" Plugins (using vim-plug) {{{1

" get vim-plug
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"   https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

filetype off
" set rtp+=~/.vim/bundle/Vundle.vim
" let path='$HOME/.vim/bundle'
" call vundle#rc('$HOME/.vim/bundle')
" call vundle#beginn()

call plug#begin('~/.vim/plugged')


" General {{{2

Plug 'terryma/vim-multiple-cursors'   " Edit multiple locations simultaneously
Plug 'tpope/vim-surround'             " Change surrounding characters quickly

" Code {{{2

Plug 'Valloric/YouCompleteMe'         " Autocompletion
Plug 'SirVer/ultisnips'                                                               " Snippet hanlder
Plug 'honza/vim-snippets'                                                     " Snippets
Plug 'pgilad/vim-skeletons'                                           " Ultisnips-compatible templates

" Writing {{{2

Plug 'vimwiki/vimwiki'                        " Personal wiki
Plug 'panozzaj/vim-autocorrect'                               " Autocorrection library

" UI {{{2

Plug 'vim-airline/vim-airline'        " Statusline
Plug 'vim-airline/vim-airline-themes'
"Plug 'ryanoasis/vim-devicons'                                        " Add icons to NERDtree, CtrlP, etc.

" Navigation {{{2

Plug 'ctrlpvim/ctrlp.vim'             " Cycle through MRUs, buffers, files fast
Plug 'scrooloose/nerdtree'            " File explorer sidebar
Plug 'majutsushi/tagbar'                                                      " Function explorer
"Plug 'lvht/tagbar-markdown'                                  " Make tagbar work with markdown
Plug 'dyng/ctrlsf.vim'                        " Search for occurrences in project

" Git {{{2

Plug 'airblade/vim-gitgutter'         " Git diff symbols in gutter
Plug 'tpope/vim-fugitive'             " Convenient git commands
Plug 'cohama/agit.vim'                                                                " Repository viewer
Plug 'jreybert/vimagit'                                                               " Repository viewer
Plug 'idanarye/vim-merginal'                                  " Git branch manager
Plug 'rhysd/git-messenger.vim'                                " Show commit message(s) relevant to current cursor position

" Markdown

Plug 'plasticboy/vim-markdown'                                " Syntax highlighting, matching rules

call plug#end()            " required
filetype plugin indent on

" Appearance {{{1

let g:airline_theme='angr'

set fillchars=fold:-
set fillchars+=vert:│

" Keymaps {{{1

" Fold with space bar
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>

" Configure Plugins {{{1

" Airline {{{2

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 0
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#obsession#enabled = 1
let g:airline_skip_empty_sections = 1
call airline#parts#define_function('codelines', 'GetNumberOfCodeLines')
call airline#parts#define_accent('codelines', 'red')
function! AirlineInit()
        " Show number of lines that aren't comments
        let g_airline_section_z = airline#section#create(['linenr', '/', 'maxlinenr', ' (', 'codelines', ')'])
        " Show current commit hash and title
        let g:airline_section_b="%{airline#util#wrap(airline#extensions#hunks#get_hunks(),0)}%{airline#util#wrap(airline#extensions#branch#get_head(),0)} (%{system('git log --oneline --no-decorate 2>/dev/null | head -1 | cut -c1-7')[:-2]})"
endfunction
autocmd User AirlineAfterInit call AirlineInit()

" Gitgutter {{{2

let g:gitgutter_enabled = 1

" Always show signcolumn - provides consistency troughout splits
if exists('&signcolumn')  " Vim 7.4.2201
        set signcolumn=yes
else
        let g:gitgutter_sign_column_always = 1
endif

" Use fontawesome icons as signs
let g:gitgutter_sign_added = ''
let g:gitgutter_sign_modified = ''
let g:gitgutter_sign_removed = ''
let g:gitgutter_sign_removed_first_line = ''
let g:gitgutter_sign_modified_removed = ''

" Set the background color of the sign column to the general background color
let g:gitgutter_override_sign_column_highlight = 1
"highlight SignColumn guibg=bg
"highlight SignColumn ctermbg=bg

" NerdTree {{{2

let NERDTreeIgnore=['\.pyc$', '\~$']
let g:NERDTreeDirArrowExpandable  = "▷"
let g:NERDTreeDirArrowCollapsible = "◢"

" pdv (PHP docstrings) {{{2
let g:pdv_template_dir = $HOME ."/.vim/bundle/pdv/templates_snip"
nnoremap <C-d> :call pdv#DocumentWithSnip()<CR>

" Undotree {{{2
" Use relative positioning to be compatible with Goyo
let g:undotree_CustomUndotreeCmd = 'vertical 32 new'
let g:undotree_CustomDiffpanelCmd= 'belowright 12 new'

" vim-markdown {{{2
" Disable immensely annoying auto-toggling of fold while typing
" Unfortunately, I haven't found a way to fix it without completely turning
" off folding
let g:vim_markdown_folding_disabled = 1
" Bullet point insertion is handled by vimwiki
let g:vim_markdown_auto_insert_bullets = 0
" This is annoyingly buggy and redundant when using vimwiki
let g:vim_markdown_new_list_item_indent = 0

" YouCompleteMe {{{2
let g:ycm_key_list_select_completion = ['<C-S-j>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-S-k>', '<Up>']
let g:ycm_key_list_accept_completion = ['<C-S-y>']
let g:ycm_complete_in_comments = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_autoclose_preview_window_after_completion = 1
" Enable YCM for non-coding environments
let g:ycm_filetype_blacklist = {'man': 1}
" Make YCM GoTo work in virtualenv (only works with versions > 8.1
"if v:version > 809
"       py << EOF
"       import os
"       import sys
"       if 'VIRTUAL_ENV' in os.environ:
"               project_base_dir = os.environ['VIRTUAL_ENV']
"               activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"               execfile(activate_this, dict(__file__=activate_this))
"               EOF
"       endif
"endif
"}}}

"######## OLD Stuff dunno ######### {{{1

" bracketed paste while in insert mode, bracketed paste preserves indentation
inoremap <silent><C-v> <Esc>:set paste<CR>a<C-r>+<Esc>:set nopaste<CR>a

" better motions with wrapped text while preserving numbered jumps
for g:key in ['k', 'j', '<Up>', '<Down>']
    execute 'noremap <buffer> <silent> <expr> ' .
                \ g:key . ' v:count ? ''' .
                \ g:key . ''' : ''g' . g:key . ''''
    execute 'onoremap <silent> <expr> ' .
                \ g:key . ' v:count ? ''' .
                \ g:key . ''' : ''g' . g:key . ''''
endfor

augroup file_load_change_and_position
    " clear this group so they don't pile up
    autocmd!

    " when quitting, save position in file
    " when re-opening go to last position
    autocmd BufReadPost * call setpos(".", getpos("'\""))

    " Reload changes if file changed outside of vim
    " requires autoread (enabled by default)
    autocmd FocusGained,BufEnter * if mode() !=? 'c' | checktime | endif
    autocmd FileChangedShellPost * echo "Changes loaded from file"
augroup END

" MODE LINE {{{1
" vim:foldmethod=marker:foldlevel=0
